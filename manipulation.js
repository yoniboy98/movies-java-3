

const BASE_URL = "https://image.tmdb.org/t/p/w500";

function createList(){
      let list = document.getElementById('list');
      let olElem = document.createElement('ul');

      for(let movie of movies.results){
            console.log(movie);

            let liElem = document.createElement('li');
            let liText = document.createTextNode(movie.original_title);
            liElem.appendChild(liText);
            liElem.setAttribute('onclick', 'getMovies(' + movie.id + ')');

            olElem.appendChild(liElem);
      }

      list.appendChild(olElem);
}

window.onload = function(){
      this.createList();
}


function getMovies(id) {
    for(let movie of movies.results) {
        if(movie.id == id){
            showDetailOfMovies(movie);
        }
    }
}


function showDetailOfMovies(movie) {
var detail = document.getElementById("detail");
detail.innerHTML = "";


let content = document.createElement("div");
let image = document.createElement("img");
let title = document.createElement("h1");
let description = document.createElement("p");
let release = document.createElement("p");
let reviews = document.createElement("p");
let langues = document.createElement("p");
let backdropcontent = document.createElement("div");
backdropcontent.setAttribute('class', 'backdrop_image');
backdropcontent.style.backgroundImage = "url(" + BASE_URL + movie.backdrop_path + ")";


let votes = document.createTextNode('votes: ' + movie.vote_count);
let votesAvg = document.createTextNode(' rating: ' + movie.vote_average);
let taal = document.createTextNode('languages: ' + movie.original_language);

image.setAttribute('src', BASE_URL + movie.poster_path);
title.appendChild(document.createTextNode(movie.original_title));
description.appendChild(document.createTextNode(movie.overview));
release.appendChild(document.createTextNode(movie.release_date));


reviews.appendChild(votes);
reviews.appendChild(votesAvg);
langues.appendChild(taal);
content.appendChild(title);
content.appendChild(backdropcontent);
content.appendChild(image);
content.appendChild(description);
content.appendChild(release);
content.appendChild(reviews);
content.appendChild(langues);

document.getElementById("detail").appendChild(content);

}


